-- Require the library
require "lsqlite3"

-- Create Tables
function createTable (DB)
  local tablesetup = [=[
    CREATE TABLE IF NOT EXISTS gamedata
    (uid INTEGER PRIMARY KEY,
     name TEXT,
     age INTEGER);
     ]=]

  DB:exec( tablesetup );
end

-- Functions that we may want to use.
function checkTableExists (DB)
  -- Just run the "CREATE IF NOT EXISTS" function.
  -- Although, this means it invisibly creates it if it doesn't.
  -- You might not want that behavior.
  createTable(DB)
  return true
end

function insertDefaultData (DB)
  local defaults = {{"mattj", 6}, {"mattc", 39}}
  for k, v in pairs(defaults) do
    local name = v[1]
    local age  = v[2]
    local insert = string.format("INSERT INTO gamedata VALUES(NULL, %q, %s);", name, age)
    print("exec: ", insert)
    result = DB:exec(insert)
    if result ~= sqlite3.OK
    then
      print("ERROR IN INSERT: ", insert)
    end
  end
end

-- For recursively printing Lua arrays.
function print_r ( t )
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            print(indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        print(indent.."["..pos.."] => "..tostring(t).." {")
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        print(indent..string.rep(" ",string.len(pos)+6).."}")
                    elseif (type(val)=="string") then
                        print(indent.."["..pos..'] => "'..val..'"')
                    else
                        print(indent.."["..pos.."] => "..tostring(val))
                    end
                end
            else
                print(indent..tostring(t))
            end
        end
    end
    if (type(t)=="table") then
        print(tostring(t).." {")
        sub_print_r(t,"  ")
        print("}")
    else
        sub_print_r(t,"  ")
    end
    print()
end

function dumpData (DB)
  for row in DB:rows('SELECT * FROM gamedata') do
    print_r(row)
  end
end

-- First, get a handle for the database file itself.
theDB = sqlite3.open("gamedata.sqlite3")

if checkTableExists(theDB)
  then
    insertDefaultData(theDB)
end

dumpData(theDB)

-- When you're done, close the file.
theDB:close()
