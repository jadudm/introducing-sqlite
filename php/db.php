<?php

// $db = new SQLite3('my_database') or die('Unable to open database');

$db = new PDO("sqlite:gamedata.sqlite3");

$command = $_GET["command"];

echo "COMMAND: $command";

if ($command == "INIT") {
  $query = <<<EOD
    CREATE TABLE IF NOT EXISTS gamedata (
      uid INTEGER PRIMARY KEY,
      name TEXT,
      age INTEGER
      )
EOD;

  $db->exec($query) or die('Create db failed');
}

if ($command == "CLEAN") {
  $db->exec("DROP * FROM TABLE gamedata");
}

if ($command == "DEFAULTS") {
  echo "INSERTING DEFAULT DATA<br>";
  $defaults = array (array("mattj", 6), array("mattc", 39));
  foreach ($defaults as $entry) {
    $insert = "INSERT INTO gamedata VALUES(NULL, '{$entry[0]}', {$entry[1]});";
    echo "INSERT: $insert<br>";
    $db->exec($insert);
  }
}

if ($command == "FETCH") {
  $result = $db->query('SELECT * FROM gamedata') or die('Query failed');
  foreach ($result as $row) {
    echo "Name: {$row[1]}<br>Age: {$row[2]}<hr>";
  }

}

?>
